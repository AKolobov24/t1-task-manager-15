package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}

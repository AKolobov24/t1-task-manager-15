package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    void clear();

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, Status status);

    List<Project> findAll();

    List<Project> findAll(Sort sort);

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}

package ru.t1.akolobov.tm.api.repository;

import ru.t1.akolobov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Integer getSize();

    Task add(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

}

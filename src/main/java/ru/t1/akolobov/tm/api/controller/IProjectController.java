package ru.t1.akolobov.tm.api.controller;

import ru.t1.akolobov.tm.model.Project;

public interface IProjectController {

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void clearProjects();

    void completeProjectById();

    void completeProjectByIndex();

    void createProject();

    void displayProjects();

    void displayProject(Project project);

    void displayProjectById();

    void displayProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

}

package ru.t1.akolobov.tm.component;

import ru.t1.akolobov.tm.api.controller.ICommandController;
import ru.t1.akolobov.tm.api.controller.IProjectController;
import ru.t1.akolobov.tm.api.controller.IProjectTaskController;
import ru.t1.akolobov.tm.api.controller.ITaskController;
import ru.t1.akolobov.tm.api.repository.ICommandRepository;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.service.ICommandService;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.api.service.ITaskService;
import ru.t1.akolobov.tm.controller.CommandController;
import ru.t1.akolobov.tm.controller.ProjectController;
import ru.t1.akolobov.tm.controller.ProjectTaskController;
import ru.t1.akolobov.tm.controller.TaskController;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.akolobov.tm.exception.system.CommandNotSupportedException;
import ru.t1.akolobov.tm.repository.CommandRepository;
import ru.t1.akolobov.tm.repository.ProjectRepository;
import ru.t1.akolobov.tm.repository.TaskRepository;
import ru.t1.akolobov.tm.service.CommandService;
import ru.t1.akolobov.tm.service.ProjectService;
import ru.t1.akolobov.tm.service.ProjectTaskService;
import ru.t1.akolobov.tm.service.TaskService;

import java.util.Scanner;

import static ru.t1.akolobov.tm.constant.ArgumentConst.*;
import static ru.t1.akolobov.tm.constant.TerminalConst.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);
    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private void initDemoData() {
        projectService.create("TEST PROJECT", Status.IN_PROGRESS);
        projectService.create("DEMO PROJECT", Status.COMPLETED);
        projectService.create("BETA PROJECT", Status.NOT_STARTED);
        projectService.create("BEST PROJECT", Status.IN_PROGRESS);
        taskService.create("TEST TASK", Status.IN_PROGRESS);
        taskService.create("DEMO TASK", Status.COMPLETED);
        taskService.create("BETA TASK", Status.NOT_STARTED);
        taskService.create("BEST TASK", Status.IN_PROGRESS);
    }

    public void run(final String[] args) {
        processArguments(args);
        initDemoData();
        commandController.displayWelcome();
        processCommands();
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_INFO:
                commandController.displayInfo();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CMD_PROJECT_DISPLAY_BY_ID:
                projectController.displayProjectById();
                break;
            case CMD_PROJECT_DISPLAY_BY_INDEX:
                projectController.displayProjectByIndex();
                break;
            case CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CMD_PROJECT_LIST:
                projectController.displayProjects();
                break;
            case CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CMD_TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CMD_TASK_LIST:
                taskController.displayTasks();
                break;
            case CMD_TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CMD_TASK_DISPLAY_BY_PROJECT_ID:
                taskController.displayTasksByProjectId();
                break;
            case CMD_TASK_DISPLAY_BY_ID:
                taskController.displayTaskById();
                break;
            case CMD_TASK_DISPLAY_BY_INDEX:
                taskController.displayTaskByIndex();
                break;
            case CMD_TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CMD_EXIT:
                exit();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void processArgument(final String arg) {
        switch (arg) {
            case ARG_VERSION:
                commandController.displayVersion();
                break;
            case ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ARG_HELP:
                commandController.displayHelp();
                break;
            case ARG_INFO:
                commandController.displayInfo();
                break;
            case ARG_ARGUMENTS:
                commandController.displayArguments();
                break;
            case ARG_COMMANDS:
                commandController.displayCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void exit() {
        System.exit(0);
    }

}

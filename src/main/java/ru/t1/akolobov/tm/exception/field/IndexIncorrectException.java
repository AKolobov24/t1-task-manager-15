package ru.t1.akolobov.tm.exception.field;

public class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is out of array range...");
    }

}

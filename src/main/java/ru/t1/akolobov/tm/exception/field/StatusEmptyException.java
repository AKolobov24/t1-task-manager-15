package ru.t1.akolobov.tm.exception.field;

public class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error! Status is empty or value does not match any picklist value...");
    }

}

package ru.t1.akolobov.tm.exception.system;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! This command is not supported.//");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command " + command + " is not supported...");
    }

}

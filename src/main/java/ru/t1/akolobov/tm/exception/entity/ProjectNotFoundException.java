package ru.t1.akolobov.tm.exception.entity;

public class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}

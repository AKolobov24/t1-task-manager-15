package ru.t1.akolobov.tm.exception.field;

public class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project Id is empty...");
    }

}

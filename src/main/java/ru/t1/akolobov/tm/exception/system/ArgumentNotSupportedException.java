package ru.t1.akolobov.tm.exception.system;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! This argument is not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument " + argument + " is not supported...");
    }

}

package ru.t1.akolobov.tm.controller;

import ru.t1.akolobov.tm.api.controller.ICommandController;
import ru.t1.akolobov.tm.api.service.ICommandService;
import ru.t1.akolobov.tm.model.Command;
import ru.t1.akolobov.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.14.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.printf("Developer: %s\n", "Alexander Kolobov");
        System.out.printf("e-mail: %s\n", "akolobov@t1-consulting.ru");
    }

    @Override
    public void displayInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + FormatUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + FormatUtil.formatBytes(usedMemory));
    }

    @Override
    public void displayCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void displayArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
